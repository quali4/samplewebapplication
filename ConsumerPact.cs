﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PactNet;

namespace UnitTests.ContractTests.ConsumerPactConfiguration
{
    public class ConsumerPact
    {
        public readonly IPactV3 pact;

        public ConsumerPact(string consumerName, string providerName)
        {
            var config = new PactConfig
            {
                PactDir = "../../../pacts/",

                DefaultJsonSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            };

            pact = Pact.V3(consumerName, providerName, config);

        }

    }
}